package main

import (
	"encoding/json"
	"fmt"
	"net/http"
	"strconv"

	"github.com/gorilla/mux"
)

type Company struct {
	CompanyName    string `json:"companyname"`
	PhoneNumber    int    `json:"phonenumber"`
	CompanyAddress string `json:"companyaddress"`
}

type Item struct {
	Name   string  `json:"name"`
	Stock  int     `json:"stock"`
	Price  int     `josn:"price"`
	Author Company `json:"author"`
}

var items = []Item{}

func main() {

	router := mux.NewRouter()

	router.HandleFunc("/item", addItem).Methods("POST")
	router.HandleFunc("/item", getAllItems).Methods("GET")
	router.HandleFunc("/item/{id}", getItem).Methods("GET")
	router.HandleFunc("/item/{id}", updateItem).Methods("PUT")
	router.HandleFunc("/item/{id}", deleteItem).Methods("DELETE")

	fmt.Println("localhost connected on port 3000")

	http.ListenAndServe(":3000", router)
}

func getItem(w http.ResponseWriter, r *http.Request) {

	var idParam string = mux.Vars(r)["id"]
	id, err := strconv.Atoi(idParam)
	if err != nil {

		w.WriteHeader(400)
		w.Write([]byte("ID must be a number"))
		return
	}

	// error checking
	if id >= len(items) {
		w.WriteHeader(404)
		w.Write([]byte("No item found with specified ID"))
		return
	}

	item := items[id]

	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(item)
}

func getAllItems(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(items)
}

func addItem(w http.ResponseWriter, r *http.Request) {

	var newItem Item
	json.NewDecoder(r.Body).Decode(&newItem)

	items = append(items, newItem)

	w.Header().Set("Content-Type", "application/json")

	json.NewEncoder(w).Encode("Success ad an item")
}

func updateItem(w http.ResponseWriter, r *http.Request) {

	var idParam string = mux.Vars(r)["id"]
	id, err := strconv.Atoi(idParam)
	if err != nil {
		w.WriteHeader(400)
		w.Write([]byte("ID must be a number"))
		return
	}

	// error checking
	if id >= len(items) {
		w.WriteHeader(404)
		w.Write([]byte("No item found with specified ID"))
		return
	}

	var updatedItem Item
	json.NewDecoder(r.Body).Decode(&updatedItem)

	items[id] = updatedItem

	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode("Item has been updated")
}

func deleteItem(w http.ResponseWriter, r *http.Request) {

	var idParam string = mux.Vars(r)["id"]
	id, err := strconv.Atoi(idParam)
	if err != nil {
		w.WriteHeader(400)
		w.Write([]byte("ID must be a number"))
		return
	}

	// error checking
	if id >= len(items) {
		w.WriteHeader(404)
		w.Write([]byte("No item found with specified ID"))
		return
	}

	items = append(items[:id], items[id+1:]...)

	w.WriteHeader(200)
	json.NewEncoder(w).Encode("Success to delete an item")
}
